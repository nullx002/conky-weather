Until 15 april 2016, I used yahoo api, but they don't allow anonymous request anymore.

So I started to write a replacement inspired reading lib{gnome,mate}weather sources.

You need to check your location code on http://weather.noaa.gov

It used to work until 1 August 2016, but as notified:
http://www.nws.noaa.gov/om/notification/scn16-16wngccb.htm

It's now outdated, anyway request can still be done through:
https://www.aviationweather.gov/metar

No idea how long it'll be accessible

I ) Usage

You can update a /home/.cache/conky/weather.txt through a cron task:
curl -s 'https://www.aviationweather.gov/metar/data?ids=LFBO&format=raw' | grep '^[A-Z]\{4\} [0-9]\{6\}Z' | sed 's/<.*$//' > ${HOME}/.cache/conky/weather.txt
The script can also be called providing location code through -l switch

This script is really easily themable, if you produce an icon set I would be glad to add it here
I'm easily reachable over freenode IRC. 

Look at the script for more details, but i'll try to update this README

Enjoy weather in conky, without looking through the window.

